# val[] = array that stores value of each object
# wt[] = array that stores weight of each object
# num = number of objects
# cap = maximum capacity of bag


def FillKnapSack(val, wt, num, cap):
    # initialize sack as sack[i][j]
    # i keeps track of num
    # j keeps track of current capacity
    # cap and num are + 1 because arrays start from 0
    sack = [[0 for j in range(cap + 1)] for i in range(num + 1)]

    # fill the sack, it will only start filling the sack after i and j are one, this is so we
    # can output 0 in the case where the sack does not get filled if the weights are too large
    for i in range(num + 1):
        for j in range(cap + 1):

            # if sack is empty, set its current value to 0
            if i == 0 or j == 0:
                sack[i][j] = 0

            # if sack is not full, input the item if the weight is less than the maximum capacity
            elif wt[i - 1] <= j:
                sack[i][j] = max(val[i - 1] + sack[i - 1][j - wt[i - 1]], sack[i - 1][j])

            # if the sack is not full, but the item is larger than maximum capacity, input the
            # values from the previous iteration
            else:
                sack[i][j] = sack[i - 1][j]

    # print max value and return sack for use in OutputKnapSack
    print("The highest possible value that can be stored in the knapsack is:", sack[num][cap])
    return sack


def OutputKnapSack(val, wt, num, cap):
    # sack gets the filled knapsack from FillKnapSack
    # sackvalue gets the maximum value from sack
    sack = FillKnapSack(val, wt, num, cap)
    sackvalue = sack[num][cap]
    length = cap

    # Decrements through the whole knapsack
    for i in range(num, 0, -1):
        # if our knapsack is emptied before our objects list is finished, we are done
        if sackvalue <= 0:
            break

        # if there is no change in the current value in sack while emptying, then keep emptying
        if sackvalue == sack[i - 1][length]:
            continue

        # if there is a change in the value in the sack, then print out the knapsack value with its weight
        else:
            print("An item with a value of", val[i - 1], "and a weight of", wt[i - 1], "is in the knapsack.")
            sackvalue = sackvalue - val[i - 1]
            length = length - wt[i - 1]


if __name__ == '__main__':
    val = [15, 17, 23]
    wt = [1, 2, 9]
    cap = 10
    num = len(val)
    OutputKnapSack(val, wt, num, cap)